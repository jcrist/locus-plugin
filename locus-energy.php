<?php
/**
Plugin Name: Locus Energy API
Description: Add Locus Energy Data using shortcode
Version: 1.1.0
Author: Jason Crist
License: PRIVATE 
Text Domain: locus-energy-api
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once 'vendor/autoload.php';

add_action('wp_loaded', 'register_scripts');
add_action('wp_enqueue_scripts', 'load_scripts');

function register_scripts(){
  wp_register_script('d3', plugin_dir_url(__FILE__) . 'd3.min.js');
  wp_register_script('moment', plugin_dir_url(__FILE__) . 'moment.min.js');
}

function load_scripts(){
  wp_enqueue_script('locus_charts', plugin_dir_url(__FILE__) . 'locus_charts.js', array('d3', 'moment'), '1.2.2');
  wp_enqueue_style('locus_charts', plugin_dir_url(__FILE__) . 'locus_charts.css', null, '1.2.2');
}

$enableCache = true;

function getAuthToken(){

  $auth_token = get_transient('locus_auth_token');

  if($auth_token){
    return $auth_token;
  }

  /*  MODIFY THESE CREDENTIALS */
  $client_id = 'b34ffed4eba12b25af35cf5b457ec369';
  $client_secret = '3f0e1e54699537cf22e202c5bcccfebc';
  $username = 'Big Rivers';
  $password = 'welcome1';
  /*  END MODIFICATIONS */

  $client = new GuzzleHttp\Client();
  $res = $client->request('POST', 'https://api.locusenergy.com/oauth/token', [
    'form_params' => [
      'grant_type' => 'password',
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'username' => $username,
      'password' => $password 
    ]
  ]);

  $auth_token = json_decode($res->getBody());

  set_transient('locus_auth_token', $auth_token, 60*60);

  return $auth_token;
}

function makeWeatherRequest($location){
  /*  MODIFY THESE CREDENTIALS */
  $openweatherkey = '4e8ec9dc20aa4c436d5a30b0b6dd0f4b';
  /*  END MODIFICATIONS */
  $url = 'http://api.openweathermap.org/data/2.5/weather?units=imperial&'.$location.'&appid='.$openweatherkey;
  $client = new GuzzleHttp\Client();
  $res = $client->request('GET', $url, [
    'headers' => [
      'Accept' => 'application/json'
    ]
  ]);
  if($res->getStatusCode() != 200){
    return null;
  }
  $response = json_decode($res->getBody());
  return $response;
}

function locus_temp_func($atts=[]){

  if(!isset($atts['location'])){
    return 'NO_LOCATION_DEFINED';
  }

  $weatherdata = makeWeatherRequest($atts['location']);
  $temp = $weatherdata->main->temp;

  return number_format($temp, 1);
}

function makeRequest($url){
  $token = getAuthToken();
  $client = new GuzzleHttp\Client();
  $res = $client->request('GET', $url, [
    'headers' => [
      'Accept' => 'application/json',
      'Authorization' => 'Bearer ' . $token->access_token
    ]
  ]);
  if($res->getStatusCode() != 200){
    return null;
  }
  $response = json_decode($res->getBody());
  return $response;
}

function getWAvg($site){

  global $enableCache;

  $value = get_transient('locus_w_avg_'.$site);

  if($value != null && $enableCache){
    return number_format($value, 2);
  }

  $now = date('Y-m-d\TH:i:s');
  $client = new GuzzleHttp\Client();
  $url = 'https://api.locusenergy.com/v3/sites/'.$site.'/data?fields=W_avg&start='.$now.'&end='.$now.'&tz=America/New_York&gran=latest';
  $response = makeRequest($url);

  if(!$response){
    return null;
  }

  $w_avg = $response->data[0]->W_avg;
  $w_avg = intval($w_avg)/1000;

  if($w_avg < 0){
    $w_avg = 0;
  }
  $w_avg = number_format($w_avg, 2);

  set_transient('locus_w_avg_'.$site, $w_avg, 60*60);

  return $w_avg;
}

function getPIAvg($site){

  global $enableCache;

  $value = get_transient('locus_pi_avg_'.$site);

  if($value != null && $enableCache){
    return $value;
  }

  $now = date('Y-m-d\TH:i:s');
  $yesterday = new DateTime(null, new DateTimeZone('America/New_York'));
  $yesterday->sub(new DateInterval('P1D'));
  $yesterdayf = $yesterday->format('Y-m-d\TH:i:s');
  $url = 'https://api.locusenergy.com/v3/sites/'.$site.'/data?fields=PI_m&start='.$yesterdayf.'&end='.$now.'&tz=America/New_York&gran=hourly&modelType=simple&irradianceSource=TMY&inverterClipping=false';

  $response = makeRequest($url);
  
  if(!$response){
    return null;
  }

  $pi_m = 0;
  if($response && $response->data && isset($response->data[0]->PI_m)){
    $pi_m = $response->data[0]->PI_m;
  }
  if($pi_m < 0){
    $pi_m = 0;
  }

  set_transient('locus_pi_avg_'.$site, $pi_m, 60*60);

  return $pi_m;
}

function getPOAIAvg($component){

  global $enableCache;

  $value = get_transient('locus_poai_avg_'.$component);

  if($value != null && $enableCache){
    return $value;
  }

  $url = 'https://api.locusenergy.com/v3/components/'.$component.'/data?gran=latest&fields=POAI_avg&tz=America/New_York';

  $response = makeRequest($url);
  
  if(!$response){
    return null;
  }

  $poai = 0;
  if($response->data && isset($response->data[0]->POAI_avg)){
    $poai = $response->data[0]->POAI_avg;
  }
  if($poai < 0){
    $poai = 0;
  }

  set_transient('locus_poai_avg_'.$component, $poai, 60*60);

  return $poai;
}

function locus_w_avg_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  if(isset($atts['site'])){
    $value = getWAvg($atts['site']);
    return $value;
  }

  $sites = explode(',', $atts['sites']);

  $data = 0;

  foreach ($sites as $site){
    $dataResponse = getWAvg($site);
    $data = $data + $dataResponse;
  }

  return $data;
}

function locus_poai_avg_func($atts=[]){
  if(!isset($atts['component'])){
    return 'NO_COMPONENT_DEFINED';
  }
  $value = getPOAIAvg($atts['component']);
  $value = number_format($value, 0);
  return $value;
}

function locus_pi_avg_func($atts=[]){

  if(!isset($atts['site'])){
    return 'NO_SITE_DEFINED';
  }

  $value = getPIAvg($atts['site']);
  return $value;
}

/* Get JSON data for power report */
function locus_wh_data($site, $gran, $interval, $startTime='now'){

  global $enableCache;

  $value = get_transient('locus_api_wh_'.$gran.'_'.$interval.'_'.$site);

  if($value != null && $enableCache){
    return $value;
  }

  $now = new DateTime($startTime, new DateTimeZone('America/New_York'));
  $nowf = $now->format('Y-m-d\TH:i:s');
  if($interval === 'ALL'){
    $yesterday = new DateTime('2017/08/08', new DateTimeZone('America/New_York'));
  } else {
    $yesterday = new DateTime($startTime, new DateTimeZone('America/New_York'));
    $yesterday->sub(new DateInterval($interval));
  }
  $yesterdayf = $yesterday->format('Y-m-d\TH:i:s');
  $url = 'https://api.locusenergy.com/v3/sites/'.$site.'/data?fields=Wh_sum&start='.$yesterdayf.'&end='.$nowf.'&tz=America/New_York&gran='.$gran;

  $response = makeRequest($url);

  if(!$response){
    return null;
  }

  set_transient('locus_api_wh_'.$gran.'_'.$interval.'_'.$site, $response->data, 60*60);

  return $response->data;

}

function locus_raw_api($params){

  global $enableCache;

  $value = get_transient('locus_api_'.$params);

//sites/[SITEID]/data?fields=Wh_sum&start=2017-01-01&end=2018-12-30&tz=America/New_York&gran=yearly';

  if($value != null && $enableCache){
    return $value;
  }

  $url = 'https://api.locusenergy.com/v3/' . $params;

  $response = makeRequest($url);

  if(!$response){
    return null;
  }

  set_transient('locus_api_wh_'.$params, $response->data, 60*60);

  return $response->data;

}

function locus_wh_total_data($partnerId, $gran, $interval){

  global $enableCache;

  $value = get_transient('locus_api_wh_'.$gran.'_'.$interval.'_'.$partnerId);

  if($value != null && $enableCache){
    return $value;
  }

  $now = new DateTime(null, new DateTimeZone('America/New_York'));
  $nowf = $now->format('Y-m-d\TH:i:s');
  $yesterday = new DateTime(null, new DateTimeZone('America/New_York'));
  $yesterday->sub(new DateInterval($interval));
  $yesterdayf = $yesterday->format('Y-m-d\TH:i:s');
  $url = 'https://api.locusenergy.com/v3/partners/'.$partnerId.'/sites/data?fields=Wh_sum&start='.$yesterdayf.'&end='.$nowf.'&tz=America/New_York&gran='.$gran;

  $response = makeRequest($url);

  if(!$response){
    return null;
  }

  set_transient('locus_api_wh_'.$gran.'_'.$interval.'_'.$partnerId, $response->data, 60*60);

  return $response->data;

}

function locus_max_hourly_func($atts=[]){

   if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  if(isset($atts['site'])){
    return locus_max_hourly_single($atts['site']);
  }

  $sites = explode(',', $atts['sites']);

  $maxValue = 0;
  foreach ($sites as $site){
    $sitemax = locus_max_hourly_single($site);
    $maxValue = $maxValue + $sitemax;
  }

  return $maxValue;
}

function locus_max_hourly_single($site){
  $maxValue = 0;
  $data = locus_wh_data($site, 'hourly', 'P1M');
  if(!$data){
    return '';
  }
  foreach($data as $item){
    $itemValue = 0;
    if(isset($item->Wh_sum)){
      $itemValue = $item->Wh_sum;
    }
    $maxValue = max($maxValue, $itemValue); 
  }
  return number_format($maxValue/1000, 2);
}

/* Build markup for chart of hourly power values */

function locus_wh_hourly_func($atts=[]){

  if(!isset($atts['site'])){
    return 'NO_SITE_DEFINED';
  }

  $data = locus_wh_data($atts['site'], 'hourly', 'P1D');

  if(!$data){
    return '';
  }

  $return = '<div class="locus_hourly">';
  foreach(array_reverse($data) as $item){
    $date = new DateTime($item->ts);
    $return = $return . '<div class="item">';
    $return = $return . '<span class="timestamp">'.$date->format('g A').' </span>';
    if(isset($item->Wh_sum)){
      $val = $item->Wh_sum;
      $val = intval($val)/1000;
    }
    else {
      $val = 0;
    }
    $return = $return . '<span class="value">'.$val.' kWh</span>';
    $return = $return . '</div>';
  }
  $return = $return . '</div>';

  return $return;

}

function locus_wh_minutely_chart_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  $elementId = 'locus_chart_'.rand();

  if(isset($atts['site'])){
    $data = locus_wh_data($atts['site'], '5min', 'P1D', 'tomorrow');
  }

  else {
    $data = array();
    $sites = explode(',', $atts['sites']);
    foreach ($sites as $site){
      $dataResponse = locus_wh_data($site, '5min', 'P1D', 'tomorrow');
      $data = array_merge($data, $dataResponse);
    }
  }

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $return = '<div class="locus_chart locus_minutely_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderMinutely('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;

}


function locus_wh_hourly_chart_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  $elementId = 'locus_chart_'.rand();

  if(isset($atts['site'])){
    $data = locus_wh_data($atts['site'], 'hourly', 'P1D', 'tomorrow');
  }

  else {
    $data = array();
    $sites = explode(',', $atts['sites']);
    foreach ($sites as $site){
      $dataResponse = locus_wh_data($site, 'hourly', 'P1D', 'tomorrow');
      $data = array_merge($data, $dataResponse);
    }
  }

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $return = '<div class="locus_chart locus_hourly_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderHourly('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;

}


function locus_wh_daily_func($atts=[]){

  if(!isset($atts['site'])){
    return 'NO_SITE_DEFINED';
  }

  $data = locus_wh_data($atts['site'], 'daily', 'P7D');

  if(!$data){
    return '';
  }

  $return = '<div class="locus_daily">';
  foreach(array_reverse($data) as $item){
    $date = new DateTime($item->ts);
    $return = $return . '<div class="item">';
    $return = $return . '<span class="timestamp">'.$date->format('l').' </span>';
    if(isset($item->Wh_sum)){
      $val = $item->Wh_sum;
      $val = intval($val)/1000;
    }
    else {
      $val = 0;
    }
    $return = $return . '<span class="value">'.$val.' kWh</span>';
    $return = $return . '</div>';
  }
  $return = $return . '</div>';

  return $return;

}

function locus_wh_daily_chart_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  $elementId = 'locus_chart_'.rand();

  if(isset($atts['site'])){
    $data = locus_wh_data($atts['site'], 'daily', 'P7D', 'tomorrow');
  }

  else {
    $data = array();
    $sites = explode(',', $atts['sites']);
    foreach ($sites as $site){
      $dataResponse = locus_wh_data($site, 'daily', 'P7D', 'tomorrow');
      $data = array_merge($data, $dataResponse);
    }
  }

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $return = '<div class="locus_chart locus_hourly_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderDaily('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;
}


function locus_wh_monthly_func($atts=[]){

  if(!isset($atts['site'])){
    return 'NO_SITE_DEFINED';
  }

  $data = locus_wh_data($atts['site'], 'monthly', 'P1Y');

  if(!$data){
    return '';
  }

  $return = '<div class="locus_daily">';
  foreach(array_reverse($data) as $item){
    $date = new DateTime($item->ts);
    $return = $return . '<div class="item">';
    $return = $return . '<span class="timestamp">'.$date->format('F').' </span>';
    if(isset($item->Wh_sum)){
      $val = $item->Wh_sum;
      $val = intval($val)/1000;
    }
    else {
      $val = 0;
    }
    $return = $return . '<span class="value">'.$val.' kWh</span>';
    $return = $return . '</div>';
  }
  $return = $return . '</div>';

  set_transient('locus_wh_montly_'.$atts['site'], $return, 60*60);

  return $return;

}

function locus_wh_monthly_chart_func($atts=[]){

  if(!isset($atts['site'])){
    return 'NO_SITE_DEFINED';
  }

  $data = locus_wh_data($atts['site'], 'monthly', 'P1Y');

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $elementId = 'locus_chart_'.rand();

  $return = '<div class="locus_chart locus_monthly_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderMonthly('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;

}

function locus_wh_yearly_chart_func($atts=[]){

  if(!isset($atts['params'])){
    return 'NO_params_DEFINED';
  }

  if(isset($atts['sites'])){
    $sites = explode(',', $atts['sites']);
    $data = array();
    foreach ($sites as $site){
      $dataResponse = locus_raw_api('sites/'.$site.'/data'. $atts['params']);
      $data = array_merge($data, $dataResponse);
    }
  } else {
    $data = locus_raw_api($atts['params']);
  }

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $elementId = 'locus_chart_'.rand();

  $return = '<div class="locus_chart locus_yearly_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderYearly('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;

}

function locus_wh_total_monthly_chart_func($atts=[]){

  if(!isset($atts['partnerid'])){
    return 'NO_PARTNER_ID_DEFINED';
  }
  
  $data = locus_wh_total_data($atts['partnerid'], 'monthly', 'P1Y');

  if(!$data){
    return '';
  }

  if(isset($atts['type'])){
    $type = $atts['type'];
  }
  else {
    $type = 'bar';
  }

  $elementId = 'locus_chart_'.rand();

  $return = '<div class="locus_chart locus_total_monthly_chart" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_charts.renderMonthly('.json_encode($data).',"#'.$elementId.'","'.$type.'");</script>'; 

  return $return;

}

function locus_icon_temp_func($atts=[]){
  $valueString = locus_temp_func($atts);
  $value = str_replace( ',', '', $valueString );
  $elementId = 'locus_icon_'.rand();
  $return = '<div class="locus_icon locus_icon_temp" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_icons.renderTemp('.$value.',"#'.$elementId.'");</script>'; 
  return $return;
}

function locus_icon_power_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  $valueString = locus_w_avg_func($atts);
  $value = str_replace( ',', '', $valueString );

  $maxValue = 100;
  if(isset($atts['max'])){
    $maxValue = $atts['max'];
  }

  $elementId = 'locus_icon_'.rand();
  $return = '<div class="locus_icon locus_icon_output" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_icons.renderOutput('.$value.','.$maxValue.',"#'.$elementId.'");</script>'; 

  return $return;
}


function locus_icon_max_func($atts=[]){

  if(!isset($atts['site']) && !isset($atts['sites'])){
    return 'NO_SITE_DEFINED';
  }

  $valueString = locus_max_hourly_func($atts);
  $value = str_replace( ',', '', $valueString );
  $maxValue = 100;
  if(isset($atts['max'])){
    $maxValue = $atts['max'];
  }

  $elementId = 'locus_icon_'.rand();
  $return = '<div class="locus_icon locus_icon_max" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_icons.renderMax('.$value.','.$maxValue.',"#'.$elementId.'");</script>'; 

  return $return;
}

function locus_icon_irrad_func($atts=[]){

  if(!isset($atts['component'])){
    return 'NO_COMPONENT_DEFINED';
  }

  $valueString = locus_poai_avg_func($atts);
  $value = str_replace( ',', '', $valueString );
  $maxValue = 300;
  if(isset($atts['max'])){
    $maxValue = $atts['max'];
  }

  $elementId = 'locus_icon_'.rand();
  $return = '<div class="locus_icon locus_icon_irrad" id="'.$elementId.'"></div>';
  $return = $return .'<script>locus_icons.renderIrrad('.$value.','.$maxValue.',"#'.$elementId.'");</script>'; 

  return $return;
}

add_shortcode( "locus_w_avg", "locus_w_avg_func" );
add_shortcode( "locus_pi_avg", "locus_pi_avg_func" );
add_shortcode( "locus_poai_avg", "locus_poai_avg_func" );
add_shortcode( "locus_wh_hourly", "locus_wh_hourly_func" );
add_shortcode( "locus_wh_daily", "locus_wh_daily_func" );
add_shortcode( "locus_wh_monthly", "locus_wh_monthly_func" );
add_shortcode( "locus_max_hourly", "locus_max_hourly_func" );

add_shortcode( "locus_temp", "locus_temp_func");

add_shortcode( "locus_wh_hourly_chart", "locus_wh_hourly_chart_func" );
add_shortcode( "locus_wh_minutely_chart", "locus_wh_minutely_chart_func" );
add_shortcode( "locus_wh_daily_chart", "locus_wh_daily_chart_func" );
add_shortcode( "locus_wh_monthly_chart", "locus_wh_monthly_chart_func" );
add_shortcode( "locus_wh_yearly_chart", "locus_wh_yearly_chart_func" );

add_shortcode( "locus_wh_total_monthly_chart", "locus_wh_total_monthly_chart_func" );

add_shortcode( "locus_icon_power", "locus_icon_power_func");
add_shortcode( "locus_icon_temp", "locus_icon_temp_func");
add_shortcode( "locus_icon_max", "locus_icon_max_func");
add_shortcode( "locus_icon_irrad", "locus_icon_irrad_func" );

?>
