(function(){

window.locus_charts = {
  renderMinutely: renderMinutely,
  renderHourly: renderHourly,
  renderDaily: renderDaily,
  renderMonthly: renderMonthly,
  renderYearly: renderYearly
};

window.locus_icons = {
  renderOutput: renderOutput,
  renderTemp: renderTemp,
  renderMax: renderMax,
  renderIrrad: renderIrrad
};

function renderMax(value, maxMax, element){

  var svgString = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg   xmlns:dc="http://purl.org/dc/elements/1.1/" preserveAspectRatio="xMinYMax meet" xmlns:cc="http://creativecommons.org/ns#"   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"   xmlns:svg="http://www.w3.org/2000/svg"   xmlns="http://www.w3.org/2000/svg"   id="svg11"   version="1.1"   height="100%"   width="100%"   viewBox="0 0 500 500"   style="isolation:isolate">  <metadata     id="metadata15">    <rdf:RDF>      <cc:Work         rdf:about="">        <dc:format>image/svg+xml</dc:format>        <dc:type           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />        <dc:title></dc:title>      </cc:Work>    </rdf:RDF>  </metadata>  <defs     id="defs5">    <clipPath       id="_clipPath_LhgEgZaMgriZR2Ryk48WdnERsKXnS0XE">      <rect         id="rect2"         height="500"         width="500" />    </clipPath>  </defs>  <g     transform="translate(5.2433766,474.15105)"     id="g35">    <path       style="fill:#2e7cc8"       id="chartLinePath"       transform="scale(0.5, 0.5)"       d="m 473.03407,-390.42584 -125,25.438 35.75,35.583 -111.479,111.625 -62.521,-62.542 -152.833007,150.875 29.375,29.541999 123.375007,-121.541999 62.312,62.355 141.271,-140.875 34.729,34.583 z" />  </g>  <g     id="g9"     clip-path="url(#_clipPath_LhgEgZaMgriZR2Ryk48WdnERsKXnS0XE)">    <path       style="fill:#2e7cc8"       d="m 500,416.667 v 41.666 H 0 V 41.667 h 41.667 v 375 z"       id="path29" />  </g></svg>';
  var svg = d3.select(element).html(svgString);

  maxMax = maxMax || 100;
  maxMax = Math.max(maxMax, value); //said sam-i-am

  var calc = d3.scaleLinear()
    .domain([0, maxMax])
    .range([0.5, 1]);

    svg.select('#chartLinePath')
      .transition().duration(2000)
        .ease(d3.easePoly)
        .attr('transform', 'scale('+calc(value)+','+calc(value)+')');
}

function renderTemp(value, element){
  var svgString ='<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg   xmlns:dc="http://purl.org/dc/elements/1.1/" preserveAspectRatio="xMinYMax meet"   xmlns:cc="http://creativecommons.org/ns#"   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"   xmlns:svg="http://www.w3.org/2000/svg"   xmlns="http://www.w3.org/2000/svg"   id="svg22"   version="1.1"   height="100%"   width="100%"   viewBox="0 0 500 500"   style="isolation:isolate">  <metadata     id="metadata26">    <rdf:RDF>      <cc:Work         rdf:about="">        <dc:format>image/svg+xml</dc:format>        <dc:type           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />        <dc:title></dc:title>      </cc:Work>    </rdf:RDF>  </metadata>  <defs     id="defs5">    <clipPath       id="tempclip">      <rect         id="tempClipPath"         transform="translate(0, 200)"         y="53.495762"         x="166.31355"         height="409.95764"         width="179.55508"/>    </clipPath>  </defs>  <g     id="g20">    <g       id="Group">      <path         id="path7"         fill="rgb(46,124,200)"         fill-rule="evenodd"         d=" M 250 0 C 209.729 0 177.083 32.646 177.083 72.917 L 177.083 229.687 C 177.083 241.917 171.708 253.542 162.396 261.458 C 133.167 286.292 114.583 323.229 114.583 364.583 C 114.583 439.375 175.187 500 250 500 C 324.812 500 385.417 439.375 385.417 364.583 C 385.417 323.229 366.833 286.292 337.604 261.458 C 328.292 253.542 322.917 241.917 322.917 229.687 L 322.917 72.917 C 322.917 32.646 290.271 0 250 0 Z  M 250 41.667 C 267.229 41.667 281.25 55.688 281.25 72.917 L 281.25 229.687 C 281.25 262.375 291.958 277.333 310.646 293.229 C 331.687 311.104 343.75 337.104 343.75 364.583 C 343.75 416.271 301.687 458.333 250 458.333 C 198.313 458.333 156.25 416.271 156.25 364.583 C 156.25 337.104 168.312 311.104 189.375 293.229 C 208.042 277.375 218.75 262.396 218.75 229.687 L 218.75 72.917 C 218.75 55.688 232.771 41.667 250 41.667 Z " />      <path         style="fill:#2e7cc8"         id="temp"         clip-path="url(#tempclip)"         d="M 293.896,309.458 C 272.813,290.917 260.417,273.333 260.417,242.375 L 259.88734,65.686441 h -20.834 L 239.583,242.375 c 0.0927,30.91686 -12.437,48.583 -33.479,67.083 -16.542,14.542 -29.021,34.209 -29.021,57.959 0,40.271 32.646,72.916 72.917,72.916 40.271,0 72.917,-32.645 72.917,-72.916 0,-23.75 -12.479,-43.396 -29.021,-57.959 z" />    </g>    <g       id="g18">      <rect         id="rect12"         fill="rgb(46,124,200)"         transform="matrix(1,0,0,1,0,0)"         height="9"         width="37"         y="192"         x="194" />      <rect         id="rect14"         fill="rgb(46,124,200)"         transform="matrix(1,0,0,1,0,0)"         height="9"         width="37"         y="134"         x="194" />      <rect         id="rect16"         fill="rgb(46,124,200)"         transform="matrix(1,0,0,1,0,0)"         height="9"         width="37"         y="76"         x="194" />    </g>  </g></svg>';
  var svg = d3.select(element).html(svgString);

  var calc = d3.scaleLinear()
    .domain([0, 100])
    .range([200, 0]);

    svg.select('#tempClipPath')
      .transition().duration(2000)
        .ease(d3.easePoly)
        .attr('transform', 'translate(0, '+calc(value)+')');
}

function renderOutput(value, maxValue, element){
  var svgString = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns:dc="http://purl.org/dc/elements/1.1/" preserveAspectRatio="xMinYMax meet" xmlns:cc="http://creativecommons.orgs#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"   style="isolation:isolate"   viewBox="0 0 500 500"   width="100%"   height="100%"   version="1.1"   id="svg50"><defs     id="defs5"><clipPath       id="_clipPath_hSVKYNMqZ0K1ODmi2kaZ5e1yO9ObB8mB"><rect         width="500"         height="500"         id="rect2" /></clipPath></defs><g     clip-path="url(#_clipPath_hSVKYNMqZ0K1ODmi2kaZ5e1yO9ObB8mB)"     id="g48"><g       clip-path="url(#_clipPath_3erMKtwN9YhaSSZ4zqbI6oORZvpylJFN)"       id="g46"><g         id="g44"><g           id="g42"><path             fill="rgb(46,124,200)"             d=" M 417.104 262.354 C 414.167 253.458 410.563 244.771 406.354 236.479 L 355.229 259.521 C 359.75 267.729 363.354 276.396 366 285.396 L 417.104 262.354 L 417.104 262.354 Z "             id="path10" /><path             fill="rgb(46,124,200)"             d=" M 155.167 243.542 C 160.812 236.167 167.25 229.479 174.312 223.604 L 134.938 182.583 C 128.104 188.708 121.667 195.396 115.813 202.521 L 155.167 243.542 L 155.167 243.542 Z "             id="path12" /><path             fill="rgb(46,124,200)"             d=" M 190.896 211.979 C 198.792 207.375 207.167 203.729 215.854 201.083 L 194.312 147.563 C 185.771 150.521 177.396 154.188 169.354 158.479 L 190.896 211.979 L 190.896 211.979 Z "             id="path14" /><path             vector-effect="non-scaling-stroke"             fill="rgb(46,124,200)"             stroke="rgb(46,124,200)"             stroke-width="1"             stroke-linecap="butt"             stroke-linejoin="miter"             d=" M 133.417 287.313 C 135.938 278.25 139.417 269.521 143.792 261.271 L 92.354 239.063 C 88.25 247.458 84.771 256.187 81.979 265.104 L 133.417 287.313 L 133.417 287.313 Z "             stroke-miterlimit="4"             id="path16" /><path             fill="rgb(46,124,200)"             d=" M 329.958 158.146 C 321.896 153.875 313.5 150.25 304.958 147.333 L 283.625 200.938 C 292.313 203.542 300.708 207.167 308.625 211.75 L 329.958 158.146 L 329.958 158.146 Z "             id="path18" /><path             fill="rgb(46,124,200)"             d=" M 384.146 202.521 C 378.271 195.396 371.854 188.708 365.021 182.583 L 325.646 223.604 C 332.708 229.479 339.125 236.187 344.771 243.542 L 384.146 202.521 L 384.146 202.521 Z "             id="path20" /><path             fill="rgb(46,124,200)"             d=" M 500 312.5 C 500 358 487.792 400.687 466.458 437.479 L 430.292 416.583 C 448.021 385.937 458.333 350.458 458.333 312.5 C 458.333 197.438 365.063 104.167 250 104.167 C 134.938 104.167 41.667 197.438 41.667 312.5 C 41.667 350.458 51.979 385.937 69.708 416.583 L 33.542 437.479 C 12.208 400.687 0 358 0 312.5 C 0 174.646 112.146 62.5 250 62.5 C 387.854 62.5 500 174.646 500 312.5 Z "             id="path23" /><g             id="Group"><g               transform="matrix(1,0,0,1,220,140.938)"               id="g27"><text                 transform="matrix(1,0,0,1,0,49.35)"   class="label_mid">50</text></g></g><g             id="g34"><g               transform="matrix(1,0,0,1,350.854,288.406)"               id="g32"><text                 transform="matrix(1,0,0,1,0,49.35)" id="text30" class="label_max">100</text></g></g><g             id="g40"><g               transform="matrix(1,0,0,1,87.885,288.406)"               id="g38"><text                 transform="matrix(1,0,0,1,0,49.35)"                 style="font-family:&quot;Lato&quot;;font-weight:900;font-size:50px;font-style:normal;fill:#2E7CC8;stroke:none;"                 id="text36">0</text></g></g><g             transform="translate(252.93302,367.03636)"             id="g3805"><path               class="dial"               d="m 1.0669753,52.51903 c -26.9579963,0 -48.8129963,-21.854 -48.8129963,-48.813 0,-18.395 10.188,-34.416 25.209,-42.729 L 1.0669753,-155.81396 24.670975,-39.04397 c 15.042,8.313 25.208995,24.334 25.208995,42.73 0,26.979 -21.854995,48.833 -48.8129947,48.833 z"               style="fill:#2e7cc8" /></g></g></g></g></g></svg>';
  var svg = d3.select(element).html(svgString);
  var dial = svg.select('.dial');
  var rotation = value;

  maxValue = Math.max(maxValue, value);

  var calc = d3.scaleLinear()
    .domain([0, maxValue])
    .range([-80, 80]);

  rotation = calc(value);

  dial.attr('transform', 'rotate(-80)');
  dial.transition().duration(2000)
    .ease(d3.easePoly)
    .attr('transform', 'rotate('+rotation+')');

  svg.select('.label_max').text(maxValue);
  svg.select('.label_mid').text(maxValue/2);
   
}

function renderIrrad(value, maxMax, element){

  var svgString = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>  <svg    preserveAspectRatio="xMinYMax meet"     xmlns:dc="http://purl.org/dc/elements/1.1/"    xmlns:cc="http://creativecommons.org/ns#"    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"    xmlns:svg="http://www.w3.org/2000/svg"    xmlns="http://www.w3.org/2000/svg"    xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"    xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"    style="isolation:isolate"    viewBox="0 0 500 500"    width="100%"    height="100%"    version="1.1"    id="svg38"    sodipodi:docname="irrad.svg"    inkscape:version="0.92.2 (5c3e80d, 2017-08-06)">   <sodipodi:namedview      pagecolor="#ffffff"      bordercolor="#666666"      borderopacity="1"      objecttolerance="10"      gridtolerance="10"      guidetolerance="10"      inkscape:pageopacity="0"      inkscape:pageshadow="2"      inkscape:window-width="2160"      inkscape:window-height="1346"      id="namedview40"      showgrid="false"      inkscape:zoom="0.944"      inkscape:cx="-234.09144"      inkscape:cy="131.143"      inkscape:window-x="-11"      inkscape:window-y="-11"      inkscape:window-maximized="1"      inkscape:current-layer="g36" />   <defs      id="defs5" />   <g      clip-path="url(#_clipPath_CxKQgLp3nHBJYDl0CIM2RR98kwaoERHW)"      id="g36">     <path        d=" M 441.5 87.927 L 381.479 147.948 C 372.917 136.948 363.021 127.052 352.021 118.49 L 412.042 58.469 L 441.5 87.927 Z "        fill="rgb(46,124,200)"        id="path7" />     <path        d=" M 270.833 84.76 L 270.833 -0.01 L 229.167 -0.01 L 229.167 84.76 C 236 83.906 242.917 83.323 250 83.323 C 257.083 83.323 264 83.906 270.833 84.76 Z "        fill="rgb(46,124,200)"        id="path9" />     <path        d=" M 415.229 229.156 C 416.083 235.99 416.667 242.927 416.667 249.99 C 416.667 257.052 416.083 263.99 415.229 270.823 L 500 270.823 L 500 229.156 L 415.229 229.156 Z "        fill="rgb(46,124,200)"        id="path11" />     <path        d=" M 375 249.99 C 375 257.094 374.271 264.031 373.125 270.823 L 126.875 270.823 C 125.729 264.031 125 257.094 125 249.99 C 125 180.948 180.958 124.99 250 124.99 C 319.042 124.99 375 180.948 375 249.99 Z "        fill="rgb(46,124,200)"        id="path13" />     <path        d=" M 330.375 229.156 C 321.042 193.323 288.688 166.656 250 166.656 C 211.292 166.656 178.958 193.323 169.646 229.156 L 330.375 229.156 Z "        fill="rgb(46,124,200)"        id="path15" />     <path        d=" M 148 118.49 L 87.979 58.469 L 58.5 87.927 L 118.521 147.948 C 127.083 136.927 136.958 127.052 148 118.49 Z "        fill="rgb(46,124,200)"        id="path17" />     <path        d=" M 0 229.156 L 0 270.823 L 84.771 270.823 C 83.917 263.99 83.333 257.052 83.333 249.99 C 83.333 242.927 83.917 235.99 84.771 229.156 L 0 229.156 Z "        fill="rgb(46,124,200)"        id="path19" />     <mask        id="_mask_Rti3BdRV3bdsk6ho4316m15Wl9yaJF6b">       <path          d=" M 188.792 472.219 C 163.687 447.323 160.375 432.781 182.292 404.302 C 190 394.281 193.313 384.677 193.313 375.135 C 193.313 355.156 178.75 335.427 159.521 312.51 L 129.354 341.24 C 158 374.573 155.312 378.156 134.771 407.573 C 128.187 417.01 125.479 426.49 125.479 435.802 C 125.479 459.948 143.729 482.865 160.167 500.01 L 188.792 472.219 Z "          fill="white"          stroke="none"          id="path21" />     </mask>     <path        d="m 188.792,472.219 c -25.105,-24.896 -28.417,-39.438 -6.5,-67.917 7.708,-10.021 11.021,-19.625 11.021,-29.167 0,-19.979 -14.563,-39.708 -33.792,-62.625 l -30.167,28.73 c 28.646,33.333 25.958,36.916 5.417,66.333 -6.584,9.437 -9.292,18.917 -9.292,28.229 0,24.146 18.25,47.063 34.688,64.208 z"        mask="url(#_mask_Rti3BdRV3bdsk6ho4316m15Wl9yaJF6b)"        stroke-miterlimit="4"        id="path24"        class="value_low"        inkscape:connector-curvature="0"        style="vector-effect:non-scaling-stroke;fill:#2e7cc8;stroke:#2e7cc8;stroke-width:4;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:5, 5" />     <mask        id="_mask_yrBqtF9rBBnBtT05VPDgT98d6tQlHZJw">       <path          d=" M 369.792 472.219 C 344.688 447.323 341.375 432.781 363.292 404.302 C 371 394.281 374.312 384.677 374.312 375.135 C 374.312 355.156 359.75 335.427 340.521 312.51 L 310.354 341.24 C 339 374.573 336.312 378.156 315.771 407.573 C 309.187 417.01 306.479 426.49 306.479 435.802 C 306.479 459.948 324.729 482.865 341.167 500.01 L 369.792 472.219 Z "          fill="white"          stroke="none"          id="path26" />     </mask>     <path        d="m 369.792,472.219 c -25.104,-24.896 -28.417,-39.438 -6.5,-67.917 7.708,-10.021 11.02,-19.625 11.02,-29.167 0,-19.979 -14.562,-39.708 -33.791,-62.625 l -30.167,28.73 c 28.646,33.333 25.958,36.916 5.417,66.333 -6.584,9.437 -9.292,18.917 -9.292,28.229 0,24.146 18.25,47.063 34.688,64.208 z"        mask="url(#_mask_yrBqtF9rBBnBtT05VPDgT98d6tQlHZJw)"        stroke-miterlimit="4"        id="path29"        class="value_high"        inkscape:connector-curvature="0"        style="vector-effect:non-scaling-stroke;fill:#2e7cc8;stroke:#2e7cc8;stroke-width:4;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:5, 5, 5, 5" />     <mask        id="_mask_J91iBwvHHgYsQWVngZGVh0l5Yf5qE60q">       <path          d=" M 278.208 472.219 C 253.104 447.323 249.792 432.781 271.708 404.302 C 279.417 394.281 282.729 384.677 282.729 375.135 C 282.729 355.156 268.167 335.427 248.938 312.51 L 218.771 341.24 C 247.417 374.573 244.729 378.156 224.187 407.573 C 217.604 417.01 214.896 426.49 214.896 435.802 C 214.896 459.948 233.146 482.865 249.583 500.01 L 278.208 472.219 Z "          fill="white"          stroke="none"          id="path31" />     </mask>     <path        d="m 278.208,472.219 c -25.104,-24.896 -28.416,-39.438 -6.5,-67.917 7.709,-10.021 11.021,-19.625 11.021,-29.167 0,-19.979 -14.562,-39.708 -33.791,-62.625 l -30.167,28.73 c 28.646,33.333 25.958,36.916 5.416,66.333 -6.583,9.437 -9.291,18.917 -9.291,28.229 0,24.146 18.25,47.063 34.687,64.208 z"        mask="url(#_mask_J91iBwvHHgYsQWVngZGVh0l5Yf5qE60q)"        stroke-miterlimit="4"        id="path34"        class="value_med"        style="vector-effect:non-scaling-stroke;fill:#2e7cc8;stroke:#2e7cc8;stroke-width:4;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:5, 5"        inkscape:connector-curvature="0" />   </g>   <metadata      id="metadata46">     <rdf:RDF>       <cc:Work          rdf:about="">         <dc:title></dc:title>       </cc:Work>     </rdf:RDF>   </metadata> </svg>'; 
  var svg = d3.select(element).html(svgString);

  maxMax = maxMax || 100;
  maxMax = Math.max(maxMax, value); //said sam-i-am

  if(value < 1){
    svg.select('.value_low')
      .attr('display', 'none');
  }

  if(value < maxMax * 0.5){
    svg.select('.value_med')
      .attr('display', 'none');
  }

  if(value < maxMax * 0.75){
    svg.select('.value_high')
      .attr('display', 'none');
  }
}

function renderDaily(data, element, type){

  var values = {};

  data.forEach(function(item){
    item.ts = item.ts.substr(0, 19);
    item.value = Math.max(0, item.Wh_sum);
    item.value = isNaN(item.value) ? 0 : item.value;
    item.label = moment(item.ts).format('ddd');
    item.label2 = moment(item.ts).format('M/D/YYYY');
    if(!values[item.ts]){
      values[item.ts] = item;
    } else {
      values[item.ts].value += item.value;
    }
  });

  var valuesArray = d3.values(values);

  renderChart (valuesArray, element, type);

}

function renderHourly(data, element, type){

  var values = {};

  data.forEach(function(item){
    item.ts = item.ts.substr(0, 19);
    var sum = item.Wh_sum || 0;
    item.value = Math.max(0, sum);
    item.value = isNaN(item.value) ? 0 : item.value;
    item.label = moment(item.ts).format('h');
    item.label2 = moment(item.ts).format('A');
    if(!values[item.ts]){
      values[item.ts] = item;
    } else {
      values[item.ts].value += item.value;
    }
  });

  var valuesArray = d3.values(values);

  renderChart (valuesArray, element, type);

}

function renderMinutely(data, element, type){

  var values = {};
  var maxSum = 0;

  data.forEach(function(item){
    item.ts = item.ts.substr(0, 19);
    var sum = item.Wh_sum || 0;
    maxSum = Math.max(maxSum, sum);
    item.value = Math.max(0, sum);
    item.value = isNaN(item.value) ? 0 : item.value;
    item.value = item.value * 12; //To get 'output-per-hour approxomation'
    var mts = moment(item.ts);
    if(mts.minute() === 0){
      item.label = mts.format('h');
      item.label2 = mts.format('A');
    }
    if(!values[item.ts]){
      values[item.ts] = item;
    } else {
      values[item.ts].value += item.value;
    }
  });

  var valuesArray = d3.values(values);

  renderChart (valuesArray, element, type, 'kW');

}

function renderMonthly(data, element, type){

  var values = {};

  data.forEach(function(item){
    item.ts = item.ts.substr(0, 19);
    item.value = Math.max(0, item.Wh_sum);
    item.value = isNaN(item.value) ? 0 : item.value;
    item.label = moment(item.ts).format('MMM');
    item.label2 = moment(item.ts).format('YYYY');
    if(!values[item.ts]){
      values[item.ts] = item;
    } else {
      values[item.ts].value += item.value;
    }
  });

  var valuesArray = d3.values(values);

  renderChart (valuesArray, element, type);

}

function renderYearly(data, element, type){
  var values = {};

  data.forEach(function(item){
    item.ts = item.ts.substr(0, 19);
    item.value = Math.max(0, item.Wh_sum);
    item.value = isNaN(item.value) ? 0 : item.value;
    item.label = moment(item.ts).format('YYYY');
    if(!values[item.ts]){
      values[item.ts] = item;
    } else {
      values[item.ts].value += item.value;
    }
  });

  var valuesArray = d3.values(values);

  renderChart (valuesArray, element, type);

}


function renderChart(data, element, type, vLabel){

  vLabel = vLabel || 'kWh';

  var chartWidth = 720;
  var chartHeight = 300;
  var marginY = 24;
  var barWidth = (chartWidth - 50)/data.length;

  var maxValue = d3.max(data, function(i){return i.value;});
  if(maxValue <= 0){
     maxValue = 10;
  }
  var roundValue = maxValue < 5000 ? 1000 : 10000;
  var topEnd = Math.ceil(maxValue/roundValue)*roundValue;
  var stepSize = Math.floor(topEnd/4); 
  topEnd += stepSize/2;
  var vlabels = [];
  for (var i=stepSize; i<=topEnd; i+=stepSize){
    vlabels.push({
      value: i,
      label: i/1000 + ' ' + vLabel
    })
  }

  var yCalc = d3.scaleLinear()
    .domain([0, topEnd])
    .range([1, chartHeight - marginY]);

  var chart = d3.select(element).append('svg')
    .attr('viewBox', '0 0 '+chartWidth+' '+chartHeight)
    .attr('preserveAspectRatio', 'xMinYMax meet');

  chart.attr('width', '100%')
    .attr('height', '100%');

  var vlabel = chart.append('g').attr('class', 'vlabels')
    .selectAll('g').data(vlabels).enter()
    .append('g')
    .attr("transform", function(d, i) { 
      return "translate("+chartWidth+", "+(chartHeight - yCalc(d.value) - marginY)+")"; 
    });

  vlabel.append("text")
    .attr('text-anchor', 'end')
    .attr('transform', 'translate(0, 10)')
    .text(function(d) { 
      return d.label; 
    })

  vlabel.append('rect')
    .attr('width', chartWidth)
    .attr('height', 1)
    .attr('transform', 'translate(' + -chartWidth + ', 0)')
    .classed('hline', true)


  if (type === "line") {

    var lineCalc = d3.line()
      .x(function(d, i){
        return i * barWidth;
      })
      .y(function(d, i){
        var yval = chartHeight - yCalc(d.value) - marginY
        return yval;
      });

    var line = chart.append('g').attr('class', 'line')
    .append('path')
    .data([data])
    .attr('d', lineCalc)

  } else {

    var bar = chart.append('g').attr('class', 'bars').selectAll('g')
    .data(data).enter()
    .append('g')
    .attr("transform", function(d, i) { 
      return "translate(" + (i * barWidth) + ", 0)"; 
    });

    bar
      .append("rect")
      .attr("width", barWidth > 1 ? barWidth - 1 : barWidth)
      .text(function(d, i) {
        return d.value / 1000 + ' ' + vLabel;
      })
      .attr("y", chartHeight - marginY)
      .transition()
      .duration(function(d) {
        return 800 + yCalc(d.value * 10);
      })
      .ease(d3.easePoly)
      .attr("height", function(d, i) {
        return yCalc(d.value);
      })
      .attr("y", function(d, i) {
        return chartHeight - yCalc(d.value) - marginY;
      });

    bar.append('title')
  }


  var hlabel = chart.append('g').attr('class', 'hlabels')
    .selectAll('g').data(data).enter()
    .append('g')
    .attr("transform", function(d, i) { 
      return "translate(" + (i*barWidth) + ", 0)"; 
    });

  hlabel.append("text")
    .attr('text-anchor', 'middle')
    .attr("x", barWidth / 2)
    .attr("y", chartHeight - 12)
    .text(function(d) { 
      return d.label; 
    });

  hlabel.append("text")
    .attr('text-anchor', 'middle')
    .attr("x", barWidth / 2)
    .attr("y", chartHeight)
    .text(function(d) { 
      return d.label2; 
    });

}


})();
